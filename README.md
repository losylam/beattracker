# Un simple beattracker en python

## Installation

Rien à faire pour la version texte.

Dépends de `matplotlib` pour afficher la courbe.

## Usage

Pour enregistrer le tempo dans un fichire `beat`
```
python3 beattracker.py
```

Pour afficher l'évolution du tempo en moyenne flottante
```
python3 beattracker_plot.py
```
la longueur de la moyenne flottante peut être changée dans le script (6 beats par défaut).
