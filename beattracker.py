#!/usr/bin/env python3
# coding: utf-8

modedemploi = """
Mode d'emploi :
  - appuyer sur [entrée] à chaque pulsation
  - appuyer sur n'importe quelle caractère puis [entrée] pour arréter le script

Les beats sont enregistrés dans le fichier beat
"""

import time
import numpy as np
import matplotlib.pyplot as plt

def get_beats():
    lst = []
    last = time.time();
    inp = ''
    input(modedemploi)
    while inp == '':
        inp = input('tap ! ')
        now = time.time()
        lst.append(now-last)
        last = now
    return lst

def main():
    lst = get_beats()[1:-1]
    lst_bpm = [60.0/i for i in lst]
    txt = ''
    for i in range(len(lst)):
        txt += '%s\t%s\n'%(lst[i], lst_bpm[i])
    with open('beat', 'w') as f:
        f.write(txt)
    return lst, lst_bpm

if __name__ == '__main__':
    main()
