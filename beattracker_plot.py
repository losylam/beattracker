#!/usr/bin/env python3
# coding: utf-8

from beattracker import main

import numpy as np
import matplotlib.pyplot as plt

AVERAGE_LENGTH = 4

if __name__ == '__main__':
    lst, lst_bpm = main()

    # running average
    running = [];
    n = AVERAGE_LENGTH
    for e in range(n, len(lst)):
        bpm = 0;
        for i in range(n):
            print(e)
            bpm += 1.0*lst_bpm[e-i]/n

        running.append(bpm)

    # plot
    plt.plot(np.array(lst[n:]).cumsum(), running)
    plt.ylim(0)
    plt.show()
